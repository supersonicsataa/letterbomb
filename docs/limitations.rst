Limitations
###########

LetterBomb cannot run on any System Menu below version **4.3**.
To exploit a **4.2** or lower Wii, look into `BannerBomb
<https://wiibrew.org/wiki/Bannerbomb>`_ or other related exploits.

LetterBomb requires:

* Any computer that has a Python installation
* Any computer with an SD card slot
* An SD/SDHC card formatted as either FAT16 or FAT32
* Your Wii's MAC address
* A Wii manufactured as RVL-001
* System Menu 4.3 *(not 4.2 or lower)*