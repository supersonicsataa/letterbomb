Tutorial
########

To fully install LetterBomb start to finish, ensure your Wii meets the requirements set out in :doc:`limitations`.

This guide also assumes you are using `WhoAteMyButter's implementation of LetterBomb
<https://gitlab.com/whoatemybutter/letterbomb>`_, and not the original website.

1. Get your Wii's MAC address and region

   * MAC: https://www.nintendo.com/consumer/systems/wii/en_na/includes/rvl-ht-int-find-mac-address.jsp
   * Region: This is where your Wii was manufactured. The first letter ID is what we want.

        +---------------+--------+-------+-------+
        | U             | E      | J     | K     |
        +===============+========+=======+=======+
        | United States | Europe | Japan | Korea |
        +---------------+--------+-------+-------+

2. Prepare SD/SDHC card

   1. Make sure **it is not** an SDXC card; it must be SD or SDHC
   2. If any data is on it, backup that data
   3. `Format the SD card
      <https://www.wikihow.com/Format-an-SD-Card>`_ to either FAT16 or FAT32

   .. warning::
    SDXC cards will not be recognized by the Wii. Only SD and SDHC cards are. Verify this before using the card.

3. Install LetterBomb

   * The easiest way to do this is by running this in a shell/terminal:

   .. code-block:: sh

       python3 -m pip install -U letterbomb

4. Ensure it's installed

   1. Open a Python shell:

   .. code-block:: sh

       python3

   2. Import LetterBomb and get its' package version

   .. code-block:: python

    import letterbomb
    print(letterbomb.__version__)

   3. Verify the version matches the version you downloaded

3. Run it through CLI

   * Exit out of the previous shell with :code:`exit()`
   * Run:

   .. code-block:: sh

       python3 -m letterbomb <MAC address from step 1.1> <region from step 1.2> -o <filename of zip> -b

   .. note::
     The ZIP archive patch can be either relative or absolute.
     :code:`..`, :code:`~`, and other shortcuts are allowed.

4. Extract resulting ZIP archive to the SD card

   * Open the archive with any compatible program (`7zip
     <https://www.7-zip.org/>`_, `WinRAR
     <https://www.rarlab.com/download.htm>`_, `Ark
     <https://apps.kde.org/en/ark>`_, etc.)
   * Extract the entire archive contents directly to the SD card

5. Verify extraction is completed

   * Ensure you extracted the exploit correctly, the card's contents should have the files & folders shown below:

     .. image:: https://i.imgur.com/4JI1MBj.png
      :width: 800

     * **Note:** *The above images' specific dates, IDs, and timestamps do not need to match;
       only the general structure*

   * Eject the SD card safely

6. Insert SD card

   * Turn off your Wii first
   * Remove the card from the computer and insert it into the Wii's SD slot until you hear a click

     * See below image on where this slot is:

       .. image:: https://i.imgur.com/4zhHmqg.gif
        :width: 400
        :align: right

7. Boot Wii and run exploit

   * Start your Wii normally
   * Navigate towards the message board
   * Scroll through the dates until you find a red mail icon with a bomb inside of it
   * Select the letter
   * Install HomeBrew and/or BootMii
