Running the web-service
#######################

While LetterBomb does not **require** a network connection, you can run your own local version of `please.hackmii.com
<http://please.hackmii.com>`_.

The web-service is written in `Flask
<https://pypi.org/project/Flask/>`_. To install it:

.. code-block:: sh

 python3 -m pip install -U flask

Once complete, put the following two lines in your code or a Python console *(either works)*:

.. code-block:: python

 import letterbomb.web
 letterbomb.web.app.run("0.0.0.0", 8080)

This will bind the service to port `8080` on all IPs assigned to the machine.

.. note::
 Although :code:`letterbomb.web` is part of :code:`letterbomb`, :code:`letterbomb.web` is not importable **through**
 :code:`letterbomb`. Instead, *it is an isolated module*.

.. note::
 To bind to a port below 1000, run the file/console with superuser priviledges.

.. warning::
 Don't use the above method for deployment servers, see `Flask docs
 <https://flask.palletsprojects.com/en/1.1.x/deploying/>`_.

Once bound, there are two ways of contacting the service:

POST (Normal)
^^^^^^^^^^^^^

Go to http://localhost:<port> (http://localhost:8080 by default).

Here is an image of it in action:

.. image:: https://i.imgur.com/yNqJJsT.png
 :width: 500
 :align: center

1. Check a region, and enter in your MAC address letter by letter.
2. If you would like to include BootMii in the LetterBomb, check the option.
3. If you configured a Captcha, verify it.
4. Cut a wire.

.. note::
 It does not matter what wire you cut.

.. warning::
 The MAC input fields only accept characters A-F, 0-9.
 Other characters are ignored, and arrows keys, etc. will still function normally.

For technical use, this is a summary of the POST request fields:

+---------------+----------------------------+---------------+----------------------+
| Field         | Description                | Expected Type | Example              |
+===============+============================+===============+======================+
| a             | 1st 2 letters of MAC       | str           | :code:`ec`           |
+---------------+----------------------------+---------------+----------------------+
| b             | 2nd 2 letters of MAC       | str           | :code:`cd`           |
+---------------+----------------------------+---------------+----------------------+
| c             | 3rd 2 letters of MAC       | str           | :code:`40`           |
+---------------+----------------------------+---------------+----------------------+
| d             | 4th 2 letters of MAC       | str           | :code:`df`           |
+---------------+----------------------------+---------------+----------------------+
| e             | 5th 2 letters of MAC       | str           | :code:`b9`           |
+---------------+----------------------------+---------------+----------------------+
| f             | Last 2 letters of MAC      | str           | :code:`a0`           |
+---------------+----------------------------+---------------+----------------------+
| region        | Character of region        | str, char     | :code:`U`, :code:`J` |
+---------------+----------------------------+---------------+----------------------+
| bootmii       | Include BootMii in archive | int, bool     | :code:`1`, :code:`0` |
+---------------+----------------------------+---------------+----------------------+

GET (JSON API)
^^^^^^^^^^^^^^

There is also an endpoint :code:`/get`, meant for plain-text HTTP GET methods.
It is very basic, and accepts *nearly* the same parameters in the above POST request, only as URL parameters.

Examples:

* :code:`/get?mac=000000000000&region=U&bootmii=1`
* :code:`/get?mac=000000000000&region=E&bootmii=0`
* :code:`/get?mac=000000000000&region=K&bootmii=1`
* :code:`/get?mac=000000000000&region=J&bootmii=0`

Guide:

+---------------+----------------------------+---------------+----------------------+
| Field         | Description                | Expected Type | Example              |
+===============+============================+===============+======================+
| mac           | MAC address of Wii         | str           | :code:`eccd40dfb9a0` |
+---------------+----------------------------+---------------+----------------------+
| region        | Character of region        | str, char     | :code:`U`, :code:`J` |
+---------------+----------------------------+---------------+----------------------+
| bootmii       | Include BootMii in archive | int, bool     | :code:`1`, :code:`0` |
+---------------+----------------------------+---------------+----------------------+

.. note::
 URL parameters look like this :code:`?first=value&second=value&third=value`

The below default-port URL should error out, telling you the MAC was invalid:
http://localhost:8080/get?mac=000000000000&region=U&bootmii=1

JSON will be sent following this inclusive format:

.. code-block:: json

 {
    "success": "boolean",
    "response": {
        "error": "str, description of error",
        "shorthand": "str, one/two words describing error"
    }
 }

.. note::
 On error, the *"success"* field will always be **false**.
 It will *never* be **true**.

On success, a file is sent. JSON will not be sent.
